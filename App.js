/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  DeviceEventEmitter
} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux'

import Home from './src/Home'
import Mine from './src/Mine'
import Example from './src/Example'
import History from './src/History'
import {isIphoneX, ifIphoneX} from './src/utils/isIphonex'
export default class App extends Component{

  constructor(props){
    super(props)
    this.state={
      selectedTab: 'home'
    }
  }

  componentDidMount(){

  }

  handleClick = (selectedTab) => {

    // this.setState({
    //   selectedTab:selectedTab
    // })
    // DeviceEventEmitter.emit('isPage',selectedTab)
   Actions.home()

    console.log(this.state.selectedTab)
    
  }


  render() {

  return (
    <ScrollView>
      <Home />
    </ScrollView>
    // <TabNavigator tabBarStyle={styles.tabBarStyle}>
    //   <TabNavigator.Item
    //     selected={this.state.selectedTab === 'home'}
    //     title="首页"
    //     renderIcon={()=><Icon name="map-marker" size={25} color="#ccc" />}
    //     renderSelectedIcon={()=><Icon name="map-marker" size={25} color="#0079ff" />}
    //     onPress={()=>this.setState({ selectedTab: 'home' })}
    //     >
    //     <Home />
    //   </TabNavigator.Item>
    
    //   <TabNavigator.Item
    //     selected={this.state.selectedTab === 'add'}
    //     title="添加"
    //     renderIcon={()=><Icon name="plus-square" size={25} color="#ccc" />}
    //     renderSelectedIcon={()=><Icon name="plus-square" size={25} color="#0079ff" />}
    //     onPress={()=>Actions.addstepone()}
    //     >
    //       <Text>
            
    //       </Text>
    //   </TabNavigator.Item>
    //   <TabNavigator.Item
    //     selected={this.state.selectedTab === 'mine'}
    //     title="我的"
    //     renderIcon={()=><Icon name="user-o" size={25} color="#ccc" />}
    //     renderSelectedIcon={()=><Icon name="user-o" size={25} color="#0079ff" />}
    //     onPress={()=>this.setState({ selectedTab: 'mine' })}
    //     >
    //       <Example />
    //   </TabNavigator.Item>
    //   <TabNavigator.Item
    //     selected={this.state.selectedTab === 'example'}
    //     title="歷史紀錄"
    //     renderIcon={()=><Icon name="map-marker" size={25} color="#ccc" />}
    //     renderSelectedIcon={()=><Icon name="map-marker" size={25} color="#0079ff" />}
    //     onPress={()=>this.setState({ selectedTab: 'example' })}
    //     >
    //     <History />
    //   </TabNavigator.Item>
    // </TabNavigator>
           
  )};
};

const styles = StyleSheet.create({
  tabBarStyle:{
    height:isIphoneX()? 74:54,
    paddingBottom:isIphoneX()?24:0
    
  }
});

