/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  DeviceEventEmitter
} from 'react-native';
import {Actions} from 'react-native-router-flux'
// import { RNCamera } from 'react-native-camera';
import { Button } from '@ant-design/react-native';

export default class Addsteptwo extends Component{

  constructor(props){
    super(props)
    this.state={
      steps1: [
        { title: 'Finished', description: 'This is description' },
        { title: 'In Progress', description: 'This is description' },
        { title: 'Waiting', description: 'This is description' },
      ],
      value:''
    }
  }

  componentDidMount(){

  }

  componentWillUnmount() {
  }

  nextStep = () => {
    Actions.addstepthree({qrcode:this.props.qrcode})
  }
  onClick = () => {
    
  }


  render() {

  return (
    <View style={styles.container}>
      <View style={{alignItems:'center',marginBottom:30}}>

      </View>
      <View style={{marginBottom:50,alignItems:'center'}}>
        <Text style={styles.fontStyle}>請按下感應器上的「啟用鈕」</Text>
        <Text style={styles.fontStyle}>和APP連線服務</Text>
      </View>
      <View style={{alignItems:'center',marginBottom:30}}>
        <Text style={styles.fontStyle} onPress={()=>this.onClick()}>感應器QR碼</Text>
        {/* <Image style={{width:200,height:200}} source={require('./assets/qrcode.jpg')} /> */}
      </View>
      <View style={{alignItems:'center',width:220}}>
        <Text style={styles.fontStyle}>感應器 ID號碼</Text>
        <Text style={{fontSize:40}}>{this.props.qrcode}</Text>
        <Button
          type="primary"
          size="lg"
          style={styles.btn}
          onPress={()=>this.nextStep()}
        >
          已成功連線到感應器
        </Button>
      </View>
      
    </View>
  )};
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    // backgroundColor:'#535353'
  },
  fontStyle: {
    // color:'#666',
    fontSize:16,
    marginBottom:10
  },
  btn: {
    marginTop:40,
    width:200,
    height:50,
    fontSize:20,
    backgroundColor:'#000'
  }
});

