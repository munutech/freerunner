
import React, { Component } from 'react'
import {isIphoneX, ifIphoneX} from './utils/isIphonex'
import { 
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Platform,
    Alert
} from 'react-native'
import {
  Button,
  Modal,
  WhiteSpace,
  WingBlank,
  Toast,
  Provider,
} from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import { Carousel } from '@ant-design/react-native';
import MapView,{PROVIDER_GOOGLE,Polyline} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {request, PERMISSIONS} from 'react-native-permissions';
import { Slider } from 'react-native-elements';

export default class Exercising extends Component {
  constructor(props){
    super(props)
    this.state = {
      value:'00:00:00',
      hour:0,
      min:0,
      second:0,
      pause:false,
      circlelatitude:0,
      circlelongitude:0,
      latitude:0,
      longitude:0,
      marginBottom:1,
      coordinates:[],
      select:0,
      slider:0,
      timeto:0
    }
  }

   componentDidMount(){
    this.aa = setInterval(this.timer,1000);
    // this.getLocationList =  setInterval(this.locateCurrentPosition(),1000);
    this.requestLocationPermission()
    // this.track()
  }

  componentWillUnmount(){
    clearInterval(this.aa)
  }


  _onMapReady = () => this.setState({marginBottom: 0})

  start = () => {
      this.setState({
          pause:false
      })
    this.aa = setInterval(this.timer,1000);
  }

  stop = () =>{
    clearInterval(this.aa)
    // clearInterval(this.getLocationList)
    Actions.result()
  }

  UNSAFE_componentWillMount(){
    clearInterval(this.aa)
  }

  pause = () => {
      this.setState({
          pause:true
      })
      clearInterval(this.aa)

  }

  timer = () => {
    
      this.setState({
        //   second:this.state.second < 10 ? '0'+this.state.second+1:this.state.second+1
        timeto:this.state.timeto +1,
          second:this.state.second+1,
          slider:(this.state.second+1)/60
      })
      if(this.state.timeto == 60 ){
        Actions.result()

      }
    if(this.state.second >= 60){
        this.setState({
            second:0,
            min:this.state.min + 1
        })
        // Actions.result()
    }
    if(this.state.min >=60){
        this.setState({
            min:0,
            hour:this.state.hour + 1
        })
    }
    this.setState({
        value:(this.state.hour<10? '0' + this.state.hour :this.state.hour) + ':' + (this.state.min<10? '0' + this.state.min :this.state.min) + ':' + (this.state.second<10? '0' + this.state.second :this.state.second)
    })

  }

  onHorizontalSelectedIndexChange(index) {
    /* tslint:disable: no-console */
    console.log('horizontal change to', index);

  }

  requestLocationPermission = async () => {
    if(Platform.OS === 'ios'){
      const response = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
      // console.log('ios'+ JSON.stringify(response))
      if(response === 'granted'){
        this.locateCurrentPosition()
        this.track()
      }
    }else{
      const response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
      // console.log('android'+ response)
      if(response === 'granted'){
        this.locateCurrentPosition()
        this.track()
      }
    }
  }



  track = () => {
    Geolocation.watchPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          coordinates: this.state.coordinates.concat({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          })
        });
        console.log('111'+this.state.coordinates[0].latitude)


      },
      error => {
        console.log(error);
      },
      {
        showLocationDialog: true,
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0,
        distanceFilter: 0
      }
    );
  }

  locateCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        let region = {
          latitude:position.coords.latitude,
          longitude:position.coords.longitude,
          latitudeDelta: 0.09,
          longitudeDelta: 0.035,          
        }
   
        this.setState({
          initialPosition:region,
          circlelatitude:position.coords.latitude,
          circlelongitude:position.coords.longitude
        })

        
      },
      error => Alert.alert(error.message),
      {timeout:1000,maximumAge:100000}
      // {enableHighAccuracy:true, timeout:10000,maximumAge:1000}
    )
  }

  onButtonClick = () => {
    Modal.alert('是否結束', '', [
      {
        text: 'Cancel',
        onPress: () => console.log('cancel'),
        style: 'cancel',
      },
      { text: 'OK', onPress: () => this.stop()},
    ]);
  };

  select = (index) => {
    this.setState({
      select:index
    })
    console.log(this.state.select)
  }
  render(){
    return (
      <Provider>
        <View style={{flex:1,width:'100%',backgroundColor: this.state.pause ? '#0c5b68' : '#232323',alignItems:'center'}}>
            <View style={{width:'100%'}}>
                <Carousel
                style={[styles.wrapper,{backgroundColor: this.state.pause ? '#0c5b68' : '#232323',}]}
                selectedIndex={this.state.select}
                infinite
                afterChange={this.onHorizontalSelectedIndexChange}
                >
                    <View>
                        <View style={{height:'80%'}}>
                            <View style={styles.time}>
                                {
                                    this.state.pause? 
                                    <Text  style={{fontSize:30,fontWeight:'bold'}}>PAUSE</Text>:
                                    <Text></Text>

                                }
                                <Text style={styles.time_num}>{this.state.value}</Text>
                                <Text style={styles.normal_text}>TIME</Text>
                                <Slider value={this.state.slider} disabled minimumTrackTintColor={'#4baec1'}
                                  trackStyle={{ width:220, height: 10, borderRadius:5 ,backgroundColor: 'transparent' }}
                                  thumbStyle={{ height: 20, width: 20, backgroundColor: 'transparent' }}
                                />
                            </View>
                            <View style={styles.distance}>
                                <Text style={styles.distance_num}>4.08</Text>
                                <Text style={styles.normal_text}>KM</Text>
                                {/* <Slider value={this.state.slider} disabled minimumTrackTintColor={'#4baec1'}
                                  trackStyle={{ width:220, height: 10, borderRadius:5 ,backgroundColor: 'transparent' }}
                                  thumbStyle={{ height: 20, width: 20, backgroundColor: 'transparent' }}
                                /> */}
                            </View>
                            <View style={{flexDirection:'row',justifyContent:'space-between',borderBottomColor:'#3b3b3b',borderBottomWidth:1}}>
                                <View style={{flex:1,alignItems:'center',borderRightWidth:1,borderRightColor:'#3b3b3b',paddingVertical:30}}>
                                    <Text style={styles.time_num}>8,415</Text>
                                    <Text style={styles.normal_text}>STEP</Text>
                                </View>
                                <View style={{flex:1,alignItems:'center',paddingVertical:30}}>
                                    <Text style={styles.time_num}>196</Text>
                                    <Text style={styles.normal_text}>CAL</Text>
                                </View>
                            </View>
                         
                          </View>
                          <View style={{width:'100%',flexDirection:'row',justifyContent:'flex-end',padding:30}}>
                            <Icon name="angle-double-right" size={40} color="#4bb0bc" />
                          </View>
                    </View>
                    <View>

                        <MapView 
                            style={{marginBottom: this.state.marginBottom,width:'100%',height:'80%'}}
                            onMapReady={this._onMapReady}
                            ref={map => this._map = map}
                            provider={PROVIDER_GOOGLE}
                            initialRegion={this.state.initialPosition}
                            showsUserLocation={true}
                            showsMyLocationButton={true}
                            zoomTapEnabled={false} 
                        >
                        <Polyline
                            coordinates={this.state.coordinates}
                            strokeColor="red" // fallback for when `strokeColors` is not supported by the map-provider
                            // strokeColors={[
                            // 	'#7F0000',
                            // 	'#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                            // 	'#B24112',
                            // 	'#E5845C',
                            // 	'#238C23',
                            // 	'#7F0000'
                            // ]}
                            strokeWidth={5}
                        />
                        </MapView> 
                        {/* <View>
                        <Text style={{color:'#fff'}}>{this.state.latitude}</Text>
                        <Text style={{color:'#fff'}}>{this.state.longitude}</Text>
                        </View> */}
                        <View style={{width:'100%',flexDirection:'row',padding:30}}>
                          <Icon name="angle-double-left" size={40} color="#4bb0bc" />
                        </View>
                    </View>
            </Carousel>
            </View>
          

            <TouchableOpacity style={{position:'absolute',bottom:30,left:40,zIndex:100}} onPress={()=>this.state.pause ? this.start() : this.pause()}>
                <View >
                    {
                        this.state.pause?
                        <Icon name="play-circle" size={100} color="#4bb0bc" />
                        :
                        <Icon name="pause-circle" size={100} color="#e2b36b" />

                    }
                </View>
            </TouchableOpacity>

            <TouchableOpacity style={{position:'absolute',bottom:30,right:40,zIndex:100}} onPress={()=>this.onButtonClick()}>
            <View>
                <Icon name="stop-circle" size={100} color="#e15f5f" />
            </View> 
            </TouchableOpacity>
            <View style={{position:'absolute',bottom:35,left:38,width:90,height:90,borderRadius:45,backgroundColor:'#fff',alignItems:'center',justifyContent:'center'}}>
            </View>
            <View style={{position:'absolute',bottom:35,right:38,width:90,height:90,borderRadius:45,backgroundColor:'#fff',alignItems:'center',justifyContent:'center'}}>
            </View>
        </View>
        </Provider>
    )
  }
}

const styles =  StyleSheet.create({
    time:{
        width:'100%',
        alignItems:'center',
        borderBottomColor:'#3b3b3b',
        borderBottomWidth:1,
        paddingBottom:10,
        paddingTop:40
    },
    time_num:{
        fontSize:35,
        color:'#fff'
    },
    normal_text:{
        color:'#808080',
        fontSize:18,
        marginBottom:10
    },
    distance:{
        width:'100%',
        padding:20,
        alignItems:'center',
        borderBottomColor:'#3b3b3b',
        borderBottomWidth:1,
    },
    distance_num:{
        color:'#fff',
        fontSize:50,
    },
    wrapper: {
        width:'100%',
        
        height:'95%',
      },
      containerHorizontal: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
      },
})
