
import React,{Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Slider,Button } from 'react-native-elements';
import {Actions} from 'react-native-router-flux'
import { Provider,Carousel,InputItem ,Modal} from '@ant-design/react-native';
import {isIphoneX, ifIphoneX} from './utils/isIphonex'
import moment from 'moment'
import {formatDate} from './utils/filter'
import constants from 'jest-haste-map/build/constants';
import History from './History';

export default class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            mobile:'',
            password:'',
            value:40,
            index:0,
            initdate:new Date(),
            stepCount:0,
            distanceCount:0,
            timeCount:0,
            caloriesCount:0,
            inputModule:0,
            visible:false,
            setValue:[
              {
                name:'step',
                value:0,
                index:0,
                unit:'step'
              },
              {
                name:'distance',
                value:0,
                index:1,
                unit:'km'
              },
              {
                name:'Calories',
                value:0,
                index:2,
                unit:'cal'
              },
              {
                name:'Time',
                value:0,
                index:3,
                unit:'min'
              },
            ],

            // date:[
            //   {
            //     day:13,
            //     sp:false
            //   },
            //   {
            //     day:14,
            //     sp:true
            //   },
            //   {
            //     day:15,
            //     sp:false
            //   },
            //   {
            //     day:16,
            //     sp:false
            //   },
            //   {
            //     day:17,
            //     sp:false
            //   },
            //   {
            //     day:18,
            //     sp:false
            //   },
            //   {
            //     day:19,
            //     sp:false
            //   }
            // ],
            date: new Date(),

        }
    }

    componentDidMount(){
      // this.initdate()
      // console.log(new Date().getDay())
    }




    onClick=(index)=>{
      this.setState({index})
    }

    onHorizontalSelectedIndexChange(index) {
      /* tslint:disable: no-console */
      console.log('horizontal change to', index);
    }

    initdate = () =>{
      let days = [] // 存放节点
      let date = new Date(this.state.initdate)
      let num = 7 - date.getDay() // 今天 分割点
      for (let i = 7 - num; i > 0; i--) { // 今天以前的日期
        let nowDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - i)
        let fmtNow = moment(nowDate).format('YYYY-MM-DD')
        console.log(fmtNow)

      }
      console.log(date)
    }
    renderInputModule = () => {
      if(this.state.inputModule == 1){
          return(
              <View style={styles.modal_content}>
                  <InputItem
                      clear
                          value={this.state.distanceCount}
                          style={styles.input}
                          placeholder="請輸入距離"
                          onChange={value => {
                          this.setState({
                              distanceCount: value,
                          });
                          }}
                      >
                  </InputItem>
                  <Button
                  containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
                  buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
                  onPress={()=>this.confirm()}>
                  </Button>
              </View>
          )
      }else if(this.state.inputModule == 2){
          return(
          <View style={styles.modal_content}>
              <InputItem
                  clear
                      value={this.state.stepCount}
                      style={styles.input}
                      placeholder="請輸入步数"
                      onChange={value => {
                      this.setState({
                          stepCount: value,
                      });
                      }}
                  >
              </InputItem>
              <Button
              containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
              buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
              onPress={()=>this.confirm()}>
              </Button>
          </View>)
      }else if(this.state.inputModule == 3){
          return(

          <View style={styles.modal_content}>
              <InputItem
                  clear
                      value={this.state.timeCount}
                      style={styles.input}
                      placeholder="請輸入時間"
                      onChange={value => {
                      this.setState({
                          timeCount: value,
                      });
                      }}
                  >
              </InputItem>
              <Button
              containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
              buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
              onPress={()=>this.confirm()}>
              </Button>
          </View> 
          )
      }else if(this.state.inputModule == 4){
          return(
          <View style={styles.modal_content}>
              <InputItem
                  clear
                      value={this.state.caloriesCount}
                      style={styles.input}
                      placeholder="請輸入卡路里數"
                      onChange={value => {
                      this.setState({
                          caloriesCount: value,
                      });
                      }}
                  >
              </InputItem>
              <Button
              containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
              buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
              onPress={()=>this.confirm()}>
              </Button>
          </View> 
          )
      }else if(this.state.inputModule == 0){
          return(
          <View>
              <Text></Text>
          </View> 
          )
      }
    }

    toggleModal = (type) => {
      this.setState({
          inputModule:type
      })
    }

    confirm = () => {
        this.setState({
            inputModule:0,
            visible:false,
        })
    }

 

    setting = () => {
      this.setState({
        visible:true
      })
    }
    renderInputModule = () => {
      if(this.state.inputModule == 1){
          return(
              <View style={styles.modal_content}>
                  <InputItem
                      clear
                          value={this.state.distanceCount}
                          style={styles.input}
                          placeholder="請輸入距離"
                          onChange={value => {
                          this.setState({
                              distanceCount: value,
                          });
                          }}
                      >
                  </InputItem>
                  <Button
                  containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
                  buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
                  onPress={()=>this.confirm()}>
                  </Button>
              </View>
          )
      }else if(this.state.inputModule == 2){
          return(
          <View style={styles.modal_content}>
              <InputItem
                  clear
                      value={this.state.stepCount}
                      style={styles.input}
                      placeholder="請輸入步数"
                      onChange={value => {
                      this.setState({
                          stepCount: value,
                      });
                      }}
                  >
              </InputItem>
              <Button
              containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
              buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
              onPress={()=>this.confirm()}>
              </Button>
          </View>)
      }else if(this.state.inputModule == 3){
          return(

          <View style={styles.modal_content}>
              <InputItem
                  clear
                      value={this.state.timeCount}
                      style={styles.input}
                      placeholder="請輸入時間"
                      onChange={value => {
                      this.setState({
                          timeCount: value,
                      });
                      }}
                  >
              </InputItem>
              <Button
              containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
              buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
              onPress={()=>this.confirm()}>
              </Button>
          </View> 
          )
      }else if(this.state.inputModule == 4){
          return(
          <View style={styles.modal_content}>
              <InputItem
                  clear
                      value={this.state.caloriesCount}
                      style={styles.input}
                      placeholder="請輸入卡路里數"
                      onChange={value => {
                      this.setState({
                          caloriesCount: value,
                      });
                      }}
                  >
              </InputItem>
              <Button
              containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
              buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
              onPress={()=>this.confirm()}>
              </Button>
          </View> 
          )
      }else if(this.state.inputModule == 0){
          return(
          <View>
              <Text></Text>
          </View> 
          )
      }
  }

  showTop = (index,i) => {
    const data = this.state.setValue.filter((item,i)=>{
      return item.index == index
    })
    const arr = this.state.setValue.splice(i,1)

    console.log(data[0])
    this.state.setValue.unshift(data[0])
    this.setState({
      setValue:this.state.setValue
    })
    console.log(this.state.setValue)

  }
  
  toStart = () => {
    const params = {
      stepCount:this.state.stepCount,
      distanceCount:this.state.distanceCount,
      timeCount:this.state.timeCount,
      caloriesCount:this.state.caloriesCount,
    }
    Actions.start({params:params})
  }
  _onPress(date) {
    this.setState({
        date
    })
}

lastWeek = () => {
    this.setState({
        date: new Date(this.state.date).getTime() - 7 * 24 * 60 * 60 * 1000
    })
}

nextWeek = () => {
    this.setState({
        date: new Date(this.state.date).getTime() + 7 * 24 * 60 * 60 * 1000
    })
}
    render() {
        const {stepCount,distanceCount,timeCount,caloriesCount,setValue} = this.state

        let days = [] // 存放节点
        let date = new Date(this.state.date)
        let num = 7 - date.getDay() // 今天 分割点
        for (let i = 7 - num; i > 0; i--) { // 今天以前的日期
            let nowDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - i)
            let fmtNow = moment(nowDate).format('YYYY-MM-DD');
            let node = <TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st]}>{nowDate.getDate()}</Text></TouchableOpacity>
            if (fmtNow === moment(date).format('YYYY-MM-DD')) {
                node = <View style={styles.curr}><TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st]}>{nowDate.getDate()}</Text></TouchableOpacity></View>
            }
            days.push(node)
        }
        for (let i = 0; i < num; i++) { // 今天以后的日期 含今天
            let nowDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + i)
            let fmtNow = moment(nowDate).format('YYYY-MM-DD');
            let node = <TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st]}>{nowDate.getDate()}</Text></TouchableOpacity>
            if (fmtNow === moment(date).format('YYYY-MM-DD')) {
                node = <View style={styles.curr}><TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st, { color: '#FFFFFF' }]}>{nowDate.getDate()}</Text></TouchableOpacity></View>
            }
            days.push(node)
        }

        return (
          <Provider>
            <ScrollView style={styles.container}>
            <View style={{alignItems:'center'}}>
                <View style={styles.bar}>
                    <Text style={{color:'#fff',}} onPress={()=>Actions.history()}>
                      歷史紀錄
                    </Text>
                    <Text style={styles.time_title}> {formatDate(new Date())} </Text>
                    <Text onPress={()=>Actions.mine()}>
                      <Icon name="user-o" size={25} color="#fff" />
                    </Text>
                </View>
                <View style={styles.card}>
                  <View style={{width:'100%',alignItems:'flex-start'}}>
                    <Text onPress={()=>this.setting()}>
                        <Icon name="cog" size={25} color="#000" />
                    </Text>
                  </View>
                  
                    {/* <View style={styles.card_title}>
                        <Text>離每日目標還差1,609步</Text>
                    </View> */}
                    <View style={{width:'100%',flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingHorizontal:30}}>
                
                      <View style={{marginRight:10}}>
                        <Slider value={0.1} disabled minimumTrackTintColor={'#4baec1'}
                            trackStyle={{ width:220, height: 10, borderRadius:5 ,backgroundColor: 'transparent' }}
                            thumbStyle={{ height: 20, width: 20, backgroundColor: 'transparent' }}
                            style={{height:10}} />
                        <Text>離每日目標還差
                          {
                            setValue[0].index == 0 ? stepCount : setValue[0].index == 1 ? distanceCount : setValue[0].index == 2 ? timeCount : caloriesCount
                          }
                        {setValue[0].unit}</Text>

                      </View>
                      <TouchableOpacity style={styles.start} onPress={()=>this.toStart()}>
                        <View >
                          <Text style={{color:'#fff'}} >立即</Text>
                          <Text style={{color:'#fff'}}>開始</Text>
                        </View>
                      </TouchableOpacity>

                    </View>
                    <View style={{flexDirection:'row',alignItems:'baseline',marginBottom:15}}>
                        <Text style={{fontSize:40,fontWeight:"bold",marginRight:5}}>{setValue[0].value}</Text>
                        <Text>{setValue[0].unit}</Text>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%'}}>
                      <TouchableOpacity style={styles.data_item} onPress={()=>this.showTop(setValue[1].index,1)}>
                        <View>
                          <Text style={styles.data_item_t}>{setValue[1].value}{setValue[1].unit}</Text>
                          <Text style={styles.data_item_b}>{setValue[1].name}</Text>
                          {/* <Text style={styles.data_item_b}>{setValue[1].unit}</Text> */}
                          <Slider value={0.1} disabled minimumTrackTintColor={'#4baec1'}
                              trackStyle={{ width:50, height: 5, borderRadius:5 ,backgroundColor: 'transparent' }}
                              thumbStyle={{ height: 20, width: 5, backgroundColor: 'transparent' }}
                              style={{height:3}} />
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.data_item} onPress={()=>this.showTop(setValue[2].index,2)}>
                        <View>
                          <Text style={styles.data_item_t}>{setValue[2].value}{setValue[2].unit}</Text>
                          <Text style={styles.data_item_b}>{setValue[2].name}</Text>
                          {/* <Text style={styles.data_item_b}>{setValue[2].unit}</Text> */}
                          <Slider value={0.1} disabled minimumTrackTintColor={'#4baec1'}
                              trackStyle={{ width:50, height: 5, borderRadius:5 ,backgroundColor: 'transparent' }}
                              thumbStyle={{ height: 20, width: 5, backgroundColor: 'transparent' }}
                              style={{height:3}} />
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.data_item} onPress={()=>this.showTop(setValue[3].index,3)}>
                        <View>
                          <Text style={styles.data_item_t}>{setValue[3].value}{setValue[3].unit}</Text>
                          <Text style={styles.data_item_b}>{setValue[3].name}</Text>
                          {/* <Text style={styles.data_item_b}>{setValue[3].unit}</Text> */}
                          <Slider value={0.1} disabled minimumTrackTintColor={'#4baec1'}
                              trackStyle={{ width:50, height: 5, borderRadius:5 ,backgroundColor: 'transparent' }}
                              thumbStyle={{ height: 20, width: 5, backgroundColor: 'transparent' }}
                              style={{height:3}} />
                        </View>
                      </TouchableOpacity>
                    </View>
                </View>
                <View style={{width:'100%',marginBottom:20,justifyContent:'space-between',flexDirection:'row',alignItems:'center'}}>
                  <Text onPress={()=>this.lastWeek()} style={{width:20}}>
                    <Icon name="angle-left" size={25} color="#fff" />
                  </Text>
                  <View>
                    <View style={{paddingHorizontal:10,width:'90%',flexDirection:'row',justifyContent:'space-around'}}>
                      <Text style={styles.time_week}>SU</Text>
                      <Text style={styles.time_week}>MO</Text>
                      <Text style={styles.time_week}>TU</Text>
                      <Text style={styles.time_week}>TR</Text>
                      <Text style={styles.time_week}>WE</Text>
                      <Text style={styles.time_week}>FR</Text>
                      <Text style={styles.time_week}>SA</Text>
                    </View>
                    <View style={[styles.weekTitleContainer, styles.fs]}>
                          {days}
                    </View>  
                  </View>
                  <Text onPress={()=>this.nextWeek()} style={{width:20}}>
                    <Icon name="angle-right" size={25} color="#fff" />
                  </Text>
                </View>
                {/* <Carousel
                    style={styles.wrapper}
                    selectedIndex={2}
                    infinite
                    dots={false}
                    afterChange={this.onHorizontalSelectedIndexChange}
                  >
                  <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',marginBottom:10}}>
                    {
                      this.state.date.map((item,index)=>{
                        return(
                          <View style={{alignItems:'center'}}>
                          <View style={{height:50}}>
                            <Text style={item.sp? styles.time_today  : this.state.index == index ? styles.time_day_sp:styles.time_day} onPress={()=>this.onClick(index)}>{item.day}</Text>
                          </View>
                          <Text style={styles.time_min}>27min</Text>
                          <Text style={styles.time_min}>67cal</Text>
                        </View>
                        )
                      })
                    }
                  </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',marginBottom:10}}>
                
                {
                  this.state.date.map((item,index)=>{
                    return(
                      <View style={{alignItems:'center'}}>
                      <View style={{height:50}}>
                        <Text style={this.state.index == index ? styles.time_day_sp:styles.time_day} onPress={()=>this.onClick(index)}>{item.day}</Text>

                      </View>
                      <Text style={styles.time_min}>27min</Text>
                      <Text style={styles.time_min}>67cal</Text>
                    </View>
                    )
                  })
                }
                </View>
                   
                </Carousel> */}


              <View style={{paddingHorizontal:20}}>
                <View style={{marginBottom:30}}>
                  <View style={{justifyContent:'space-between',flexDirection:'row'}}>
                    <Text style={styles.time_week}>今日行程紀錄
                    </Text>
                    <Text onPress={()=>Actions.start({typeone:1})}>
                      <Icon name="plus-square" size={25} color="#fff" />
                    </Text>
                  </View>
                  <View style={styles.stroke_item}>
                    <Text style={styles.stroke_item_text}>11:30-12:30</Text>
                    <Text style={styles.stroke_item_text}>騎單車</Text>
                    <Text style={styles.stroke_item_text}>60min</Text>
                  </View>
                  <View style={styles.stroke_item}>
                    <Text style={styles.stroke_item_text}>11:30-12:30</Text>
                    <Text style={styles.stroke_item_text}>騎單車</Text>
                    <Text style={styles.stroke_item_text}>60min</Text>
                  </View>
                  <View style={styles.history_item}>
                    <Text style={styles.stroke_item_text}>11:30-12:30</Text>
                    <Text style={styles.stroke_item_text}>騎單車</Text>
                    <Text style={styles.stroke_item_text}>60min <Icon name="chevron-right" size={15} color="#676767" />
                    </Text>
                  </View>
                </View>
                {/* <View style={{alignItems:'flex-start',marginBottom:30}}>
                  <Text style={styles.time_week}>今日運動紀錄</Text>
                  <View style={styles.history_item}>
                    <Text style={styles.stroke_item_text}>11:30-12:30</Text>
                    <Text style={styles.stroke_item_text}>騎單車</Text>
                    <Text style={styles.stroke_item_text}>60min <Icon name="chevron-right" size={15} color="#676767" />
                    </Text>
                  </View>
                </View> */}
                </View>
               
            <Modal
              title="Title"
              transparent
              onClose={this.confirm}
              maskClosable
              visible={this.state.visible}
              closable
              style={{alignItems:'center',width:300}}
              // footer={footerButtons}
            >


              <View style={{ paddingVertical: 20 }}>
                <View style={styles.mask_bottom}>
                      <View style={styles.mask_bottom_item}>
                          <TouchableOpacity style={styles.activity} onPress={()=>this.toggleModal(1)}>  
                                  <Icon name="male" size={40} color={this.state.distanceCount? '#4bb0bc': '#ccc'} />
                                  <View style={{alignItems:'center'}}>
                                      <Text>Distance</Text>
                                      <Text style={styles.countStyle}>{this.state.distanceCount}</Text>
                                  </View>

                          </TouchableOpacity>
                          <TouchableOpacity style={styles.distance} onPress={()=>this.toggleModal(2)}>  
                              <Icon name="male" size={40} color={this.state.stepCount? '#4bb0bc': '#ccc'} />
                              <View style={{alignItems:'center'}}>
                                  <Text>Step</Text>
                                  <Text style={styles.countStyle}>{this.state.stepCount}</Text>
                              </View>
                          </TouchableOpacity>
                      </View>
                      <View style={styles.mask_bottom_item}>
                          <TouchableOpacity style={styles.time} onPress={()=>this.toggleModal(3)}>  
                              <Icon name="male" size={40} color={this.state.timeCount? '#4bb0bc': '#ccc'} />
                              <View style={{alignItems:'center'}}>
                                  <Text>Time</Text>
                                  <Text style={styles.countStyle}>{this.state.timeCount}</Text>
                              </View>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.calories} onPress={()=>this.toggleModal(4)}>  
                              <Icon name="male" size={40} color={this.state.caloriesCount? '#4bb0bc': '#ccc'} />
                              <View style={{alignItems:'center'}}>
                                  <Text>Calories</Text>
                                  <Text style={styles.countStyle}>{this.state.caloriesCount}</Text>
                              </View>
                          </TouchableOpacity>
                      </View>
              </View>
                {this.renderInputModule()}
              </View>
           
            </Modal>
            </View>

            </ScrollView>
          </Provider>
          );
    }
  }

const styles = StyleSheet.create({
    container: {
      flex: 1,
      // paddingTop:isIphoneX()?40:0,
      paddingTop:40,
      backgroundColor: '#232323',
      padding:20,
    },
    data_item_b:{
      marginBottom:3
    },
    bar: {
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        // backgroundColor:'#4baec1',
        marginBottom:30,
        alignItems:'center'
    },
    time_title: {
        fontSize:20,
        color:'#fff'
    },
    card:{
        width:'100%',
        backgroundColor:'#fff',
        paddingHorizontal:10,
        paddingTop:10,
        paddingBottom:20,
        alignItems:'center',
        borderRadius:10,
        marginBottom:30,
    },
    card_title:{
        flexDirection:'row',
        marginBottom:20
    },
    sp_word:{
      marginLeft:5,
      width:80,
      height:20,
      borderRadius:10,
      textAlign:'center',
      color:'#4bb0bc',
      borderColor:'#4bb0bc',
      borderWidth:1
    },
    step:{

    },
    data_item:{
      alignItems:'center'
    },
    data_item_t:{
      fontSize:25,
      fontWeight:"bold"
    },
    time_week:{
      color:'#43b4c2',
      fontSize:20,
      fontWeight:"bold",
      marginBottom:15
    },
    time_day:{
      color:'#43b4c2',
      fontSize:27,
      fontWeight:"400",
      marginBottom:5,
      textAlign:'center',
      width:40,
      height:40,
      lineHeight:40,
    },
    time_today:{
      textAlign:'center',
      width:40,
      height:40,
      fontSize:27,
      borderRadius:21,
      overflow: 'hidden',
      fontWeight:"400",
      marginBottom:5,
      lineHeight:40,
      borderColor:'#999',
      borderWidth:1,
      color:'#4bafbf'
    },
    time_day_sp:{
      textAlign:'center',
      width:40,
      height:40,
      fontSize:27,
      borderRadius:20,
      overflow: 'hidden',
      fontWeight:"400",
      marginBottom:5,
      lineHeight:40,
      backgroundColor:'#4bafbf',
      color:'#fff'
    },
    time_day_sp_point:{
      textAlign:'center',
      width:34,
      height:34,
      fontSize:27,
      borderRadius:15,
      overflow: 'hidden',
      fontWeight:"400",
      marginBottom:2,
      backgroundColor:'#4bafbf',
      color:'#fff'
    },
    point:{
      width:8,
      height:8,
      borderRadius:4,
      overflow:'hidden',
      backgroundColor:'#ccc',
      position:'absolute',
      top:45,
      right:5,
    },
    time_min:{
      color:'#676767'
    },
    stroke_item:{
      width:'100%',
      paddingHorizontal:20,
      paddingVertical:10,
      backgroundColor:'#43b4c2',
      justifyContent:'space-between',
      flexDirection:'row',
      marginBottom:10,
      alignItems:'center'
    },
    stroke_item_text:{
      color:'#fff',
      fontSize:18
    },
    // history_item:{
    //   width:'100%',
    //   flexDirection:'row',
    //   justifyContent:'space-between',
    //   borderBottomColor:'#676767',
    //   borderBottomWidth:1,
    //   paddingVertical:10,
    //   alignItems:'center',
    //   backgroundColor:'#999'
    // },
    history_item:{
      width:'100%',
      paddingHorizontal:20,
      paddingVertical:10,
      backgroundColor:'#999',
      justifyContent:'space-between',
      flexDirection:'row',
      marginBottom:10,
      alignItems:'center'
    },
    start:{
      width:44,
      height:44,
      borderRadius:22,
      backgroundColor:'#43b4c2',
      alignItems:'center',
      justifyContent:'center'
    },
    wrapper: {
      backgroundColor: '#232323',
      width:'100%',
      height:100
    },
    containerHorizontal: {
      flexGrow: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 150,
    },
    //底部setting
    mask_bottom:{
      backgroundColor:'#fff',
      height:180,
      width:'100%',
      borderRadius:10,
      borderColor:'#4bb0bc',
      borderWidth:1
  },
  mask_bottom_item:{
      flexDirection:'row'
  },
  activity:{
      justifyContent:'space-around',
      flexDirection:'row',
      alignItems:'center',
      height:90,
      padding:20,
      width:'50%',
      borderBottomWidth:1,
      borderRightWidth:1,
      borderColor:'#4bb0bc'
  },
  time:{
      justifyContent:'space-around',
      flexDirection:'row',
      alignItems:'center',
      height:90,
      padding:20,
      width:'50%',
      borderRightWidth:1,
      borderColor:'#4bb0bc'
  },
  distance:{
      justifyContent:'space-around',
      flexDirection:'row',
      alignItems:'center',
      height:90,
      padding:20,
      width:'50%',
      borderBottomWidth:1,
      borderColor:'#4bb0bc'
  },
  calories:{
      justifyContent:'space-around',
      flexDirection:'row',
      alignItems:'center',
      height:90,
      padding:20,
      width:'50%',
  },
  modal_content:{
      marginTop:10,
      width:'100%',
      paddingHorizontal:20,
      alignItems:'center',
      backgroundColor:"#fff",
  },
  btn:{
      marginTop:20,
      height:50,
      fontSize:20,
      backgroundColor:'#4bb0bc',
      color:'#fff'
  },
  countStyle:{
      color:'#4bb0bc',
      fontSize:30,
      fontWeight:'bold',
  },
  input:{
    width:'100%'
  },
  weekTitleContainer: {
    height: 27,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom:10,
    marginHorizontal:20,
    width:'80%'
  },
  fs: {
      fontSize: 14,
      color:'#fff'
  },
  curr: {
    borderRadius: 24,
    backgroundColor: '#316DE6',
    width: 24,
    height: 24,
    textAlign: 'center',
    lineHeight: 24,
  },
  st: {
    width: 24,
    height: 24,
    textAlign: 'center',
    lineHeight: 24,
  },
  space: {
    flex: 1,
    paddingTop: 10,
    paddingHorizontal: 30,
    paddingBottom: 10
  }
});
