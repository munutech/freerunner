/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  DeviceEventEmitter
} from 'react-native';
import {Actions} from 'react-native-router-flux'

import { InputItem } from '@ant-design/react-native';

export default class Addstepone extends Component{

  constructor(props){
    super(props)
    this.state={
      steps1: [
        { title: 'Finished', description: 'This is description' },
        { title: 'In Progress', description: 'This is description' },
        { title: 'Waiting', description: 'This is description' },
      ],
      value:'',
      inputValue: ''
    }
  }

  componentDidMount(){
    const that = this
    this.listener = DeviceEventEmitter.addListener('isEditNotice',(params)=>{
      if(params == 1){
        this.toStep()
      }
    });

    this.setState({
      inputValue:this.props.barCodeData?this.props.barCodeData:''
    })

  }

  componentWillUnmount() {
    if(this.listener){
      this.listener.remove()

    }
  }

  toStep = () => {
    Actions.addsteptwo({qrcode:this.state.inputValue})

  }
  // componentWillReceiveProps(nextProps) {
  //   console.log(222222222222222)
  //   console.log(nextProps.barCodeData)
  //   this.setState({
  //     inputValue: barCodeData.typeValue
  //   })
    // 监听扫码页面获取的值
    // let { barCodeData } = nextProps.barCodeData
    // let that = this
    // if(barCodeData && barCodeData.typeValue && barCodeData.typeName == 'testScan') {
    //     that.setState({
    //         inputValue: barCodeData.typeValue
    //     })
    // }
  // }

  handleScanCheck() {
      Actions.scan()
  }


  render() {

  return (
    <View style={styles.container}>
      {/* <View style={{alignItems:'center'}}>
        <Steps size="small" current={0} direction="horizontal">
          <Step title='1' />
          <Step title='1' />
          <Step title='1' />
        </Steps>
      </View> */}
      <View style={{marginBottom:50,alignItems:'center'}}>
        <Text style={styles.fontStyle}>請將感應器QR碼對準於框內</Text>
        <Text style={styles.fontStyle}>或在下方輸入感應器ID條碼</Text>
      </View>
      <View style={{alignItems:'center',marginBottom:30}}>
        <Text style={styles.fontStyle} onPress={()=>this.handleScanCheck()}>感應器QR碼</Text>
        {/* <Image style={{width:200,height:200}} source={require('./assets/qrcode.jpg')} /> */}
      </View>
      <View style={{alignItems:'center',width:200}}>
        <Text style={styles.fontStyle}>感應器 ID號碼</Text>
        <InputItem type='number' maxLength={8} style={{fontSize:35,color:'#fff',height:100,textAlign:'auto',textAlignVertical: "center"}}
            value={this.state.inputValue}
            onChange={value => {
              this.setState({
                inputValue: value,
              });
            }}
        />
      </View>
    </View>
  )};
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    backgroundColor:'#535353'
  },
  fontStyle: {
    color:'#fff',
    fontSize:16,
    marginBottom:10
  }
});

