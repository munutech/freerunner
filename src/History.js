
import React,{Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input,ButtonGroup } from 'react-native-elements';
import {Actions} from 'react-native-router-flux'
import Echarts from 'native-echarts';
import { Provider} from '@ant-design/react-native';
import {isIphoneX, ifIphoneX} from './utils/isIphonex'

export default class History extends Component{
    constructor () {
        super()
        this.state = {
          selectedIndex: 2
        }
        this.updateIndex = this.updateIndex.bind(this)
      }
      
      updateIndex (selectedIndex) {
        this.setState({selectedIndex})
      }
      
      render () {
        const buttons = ['D', 'W', 'M','Y']
        const { selectedIndex } = this.state
        const option = {
          title: {
              text: 'ECharts demo'
          },
          tooltip: {},
          legend: {
              data:['销量']
          },
          xAxis: {
              data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
          },
          yAxis: {},
          series: [{
              name: '销量',
              type: 'bar',
              data: [5, 20, 36, 10, 10, 20]
          }]
        };
        return (
          <View style={styles.container}>
            <ButtonGroup
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              buttons={buttons}
              containerStyle={{height: 40}}
            />
            <View style={{flex:1,width:"100%",backgroundColor:'#ccc'}}>
              <Echarts option={option} height={300} />
            </View>
          </View>

        )
      }
  }


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      paddingTop:isIphoneX()?40:0,
    },

});
