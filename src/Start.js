import React, { Component } from 'react';
import {
    Modal,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
  Dimensions
} from 'react-native';
import { Slider, Button, ButtonGroup } from 'react-native-elements';
import {isIphoneX, ifIphoneX} from './utils/isIphonex'
import Icon from 'react-native-vector-icons/FontAwesome';
import { WhiteSpace, InputItem, Provider } from '@ant-design/react-native';
import MapView from 'react-native-maps';
import {formatDate} from './utils/filter'
import { Actions } from 'react-native-router-flux';
import Carousel from 'react-native-snap-carousel';
import DatePicker from 'react-native-datepicker'

export default class Start extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status:1,// 0正常页面 1倒数页面 2暂停 
            modalVisible: true,
            count:10,
            stepCount:0,
            distanceCount:0,
            timeCount:0,
            caloriesCount:0,
            inputModule:0,
            date:new Date(),
            typeone:this.props.typeone,
            isBtn:0,//0 没有立即开始按钮 1 有立即开始按钮
            entries:[
                {
                    image:require('../src/assets/bg.jpg'),
                    title:'跑步'
                },
                {
                    image:require('../src/assets/bg.jpg'),
                    title:'走路'
                },
                {
                    image:require('../src/assets/bg.jpg'),
                    title:'游泳'
                },
                {
                    image:require('../src/assets/bg.jpg'),
                    title:'跑步'
                },
                {
                    image:require('../src/assets/bg.jpg'),
                    title:'走路'
                },
                {
                    image:require('../src/assets/bg.jpg'),
                    title:'游泳'
                },
            ]
        }
    }

    componentDidMount(){
        console.log(this.props.typeone)
        //如果从+按钮点进来此页面
        if(this.state.typeone === 1){
            this.setState({
                status: 1,

            })
        }else{
            this.setState({
                stepCount:this.props.params.stepCount?this.props.params.stepCount:0,
                distanceCount:this.props.params.distanceCount?this.props.params.distanceCount:0,
                timeCount:this.props.params.timeCount?this.props.params.timeCount:0,
                caloriesCount:this.props.params.caloriesCount?this.props.params.caloriesCount:0,
            })
            this.startNow(0)
        }
        // if(this.props.params){
        //     if(this.props.params.stepCount || this.props.params.distanceCount || this.props.params.timeCount || this.props.params.caloriesCount){
        //         this.setState({
        //             isBtn:1
        //         })
        //     }
        // }


        console.log(this.props.params)
    }

    // componentWillUpdate(){
    //     clearInterval(this.aa)
    // }

    componentWillUnmount(){
        clearInterval(this.aa)
    }
 
    setModalVisible = ()=> {
        if(this.state.typeone === 1){
            this.setState({
                status: 1,
            })
        }else{
            const {count} = this.state;
            if (count === 1) {
            this.setState({
                count: 10,
                status: 0,
            });
            Actions.exercising()
            } else {
            this.setState({
                count: count - 1,
                status: 1,
            });
            this.aa = setTimeout(this.setModalVisible.bind(this), 1000);
    
            }
        }


    }
    toggleModal = (type) => {
        this.setState({
            inputModule:type
        })
    }

    confirm = () => {
        this.setState({
            inputModule:0,
            status: 1,
            isBtn:1,
            count:10

        })
        clearInterval(this.aa)

    }
    _renderItem = ({item, index}) => {
        return (
            <View style={styles.carouselStyle}>
                <Text style={styles.carouseTitle}>{ item.title }</Text>
                <Image style={styles.carouseImage} source={item.image} />
            </View>
        );
    }

    startNow = (typeone) =>{
        this.setState({
            isBtn:typeone,
            typeone,
            count:10
        })
        const {count} = this.state;
        if (count === 1) {
        this.setState({
            count: 10,
            status: 0,
        });
        } else {
        this.setState({
            count: count - 1,
            status: 1,
        });
        setTimeout(this.setModalVisible.bind(this), 1000);

        }
        
    }

    stop = () => {
        this.setState({
            status:2
        })
    }

    continue = () => {
        this.setState({
            status:0,
        }) 
    }

    renderInputModule = () => {
        if(this.state.inputModule == 1){
            return(
                <View style={styles.modal_content}>
                    <InputItem
                        clear
                            value={this.state.distanceCount}
                            style={styles.input}
                            placeholder="請輸入距離"
                            onChange={value => {
                            this.setState({
                                distanceCount: value,
                            });
                            }}
                        >
                    </InputItem>
                    <Button
                    containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
                    buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
                    onPress={()=>this.confirm()}>
                    </Button>
                </View>
            )
        }else if(this.state.inputModule == 2){
            return(
            <View style={styles.modal_content}>
                <InputItem
                    clear
                        value={this.state.stepCount}
                        style={styles.input}
                        placeholder="請輸入步数"
                        onChange={value => {
                        this.setState({
                            stepCount: value,
                        });
                        }}
                    >
                </InputItem>
                <Button
                containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
                buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
                onPress={()=>this.confirm()}>
                </Button>
            </View>)
        }else if(this.state.inputModule == 3){
            return(

            <View style={styles.modal_content}>
                <InputItem
                    clear
                        value={this.state.timeCount}
                        style={styles.input}
                        placeholder="請輸入時間"
                        onChange={value => {
                        this.setState({
                            timeCount: value,
                        });
                        }}
                    >
                </InputItem>
                <Button
                containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
                buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
                onPress={()=>this.confirm()}>
                </Button>
            </View> 
            )
        }else if(this.state.inputModule == 4){
            return(
            <View style={styles.modal_content}>
                <InputItem
                    clear
                        value={this.state.caloriesCount}
                        style={styles.input}
                        placeholder="請輸入卡路里數"
                        onChange={value => {
                        this.setState({
                            caloriesCount: value,
                        });
                        }}
                    >
                </InputItem>
                <Button
                containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
                buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='確認' 
                onPress={()=>this.confirm()}>
                </Button>
                <Text style={{position:'absolute',top:5,right:5 }}>
                 <Icon name="close-o" size={40} color={this.state.distanceCount? '#4bb0bc': '#ccc'} />
                </Text>
            </View> 
            )
        }else if(this.state.inputModule == 0){
            return(
            <View>
                <Text></Text>
            </View> 
            )
        }
    }

    renderMask = () => {
        if(this.state.status == 2){
            return(
                <View style={{flex:1,position:'absolute',width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0.6)',zIndex:100,paddingTop:isIphoneX()?40:0,alignItems:'center',paddingHorizontal:20}}>
                    <View style={styles.card}>
                        {/* <View style={styles.card_title}>
                            <Text>離每日目標還差1,609步</Text>
                            <Text style={styles.sp_word} onPress={() => this.setModalVisible()}>Start Now</Text>
                        </View> */}
                        <View style={{width:'100%',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
                        <View style={{width:200}}>
                            <Slider value={0.1} disabled minimumTrackTintColor={'#4baec1'} thumbStyle={{height:0}}/>
                        </View>
                        </View>
                        <View style={{flexDirection:'row',alignItems:'baseline',marginBottom:15}}>
                            <Text style={{fontSize:25,fontWeight:"bold",marginRight:5}}>6,391</Text>
                            <Text>step</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%'}}>
                        <View style={styles.data_item}>
                            <Text style={styles.data_item_t}>1.2</Text>
                            <Text style={styles.data_item_b}>distance</Text>
                            <Text style={styles.data_item_b}>(km)</Text>
                        </View>
                        <View style={styles.data_item}>
                            <Text style={styles.data_item_t}>43</Text>
                            <Text style={styles.data_item_b}>Calories</Text>
                            <Text style={styles.data_item_b}>(cal)</Text>
                        </View>
                        <View style={styles.data_item}>
                            <Text style={styles.data_item_t}>37</Text>
                            <Text style={styles.data_item_b}>Time</Text>
                            <Text style={styles.data_item_b}>(min)</Text>
                        </View>
                        </View>
                    </View>                   
                    <View style={{marginTop:30,alignItems:'center'}}>
                        <Text style={styles.close} onPress={() => this.setModalVisible()}>PAUSE</Text>
                    </View>
                    <View style={{position:'absolute',bottom:30,left:40}}>
                        <Button
                        containerStyle={{width:100,backgroundColor:'#2c4b50'}}
                        titleStyle={{color:'#fff'}}
                        title="STOP"
                        onPress={()=>this.continue()}
                        type="outline"
                        />
                    </View>
                    <View style={{position:'absolute',bottom:30,right:40}}>
                        <Button
                        containerStyle={{width:100,backgroundColor:'#efb166'}}
                        titleStyle={{color:'#fff'}}
                        title="PAUSE"
                        type="outline"
                        />
                    </View>
                </View>
            )
        }else if(this.state.status == 1){
            return(
                <View style={{flex:1,position:'absolute',width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0.6)',zIndex:100,paddingTop:isIphoneX()?40:0,alignItems:'center',paddingHorizontal:20}}>
                {
                this.state.typeone != 1?
                    this.state.isBtn == 1 ?
                        <TouchableOpacity style={{marginTop:40}} onPress={()=>this.startNow(0)}>  
                            <View style={styles.now_start}>
                                <Text style={{fontSize:30,color:'#fff'}}>
                                    立即
                                </Text>
                                <Text style={{fontSize:30,color:'#fff'}}>
                                    开始
                                </Text>
                            </View>
                        </TouchableOpacity>
                        :
                        <View style={{position:'absolute',top:'20%',alignItems:'center'}}>
                            <Text style={styles.close} onPress={() => this.setModalVisible()}>{this.state.count}</Text>
                            <Text style={{color:'#fff',fontSize:20}} onPress={() => this.setModalVisible()}>Tap to start immediately</Text>
                        </View>
                :
                <View style={{width:'100%',alignItems:'center',marginBottom:20}}>
                    <DatePicker
                        style={{width: 250,fontSize:20}}
                        date={this.state.date}
                        mode="datetime"
                        placeholder="select date"
                        format="YYYY-MM-DD, h:mm a"
                        minDate={new Date()}
                        maxDate="2099-12-31"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36,
                            borderColor:'#4bb0bc',
                            borderRadius:10
                        },
                        dateText:{
                            color:'#4bb0bc',
                            fontSize:20
                        }
                        // ... You can check the source to find the other keys.
                        }}
                        onDateChange={(date) => {this.setState({date: date})}}
                    />
                    <TouchableOpacity style={{marginTop:40}} onPress={()=>this.startNow(0)}>  
                        <View style={styles.now_start}>
                            <Text style={{fontSize:30,color:'#fff'}}>
                                立即
                            </Text>
                            <Text style={{fontSize:30,color:'#fff'}}>
                                开始
                            </Text>
                        </View>
                    </TouchableOpacity>
                 </View>
                }

                    <Carousel
                    firstItem={1}
                    ref={(c) => { this._carousel = c; }}
                    data={this.state.entries}
                    containerCustomStyle={styles.carousel}
                    renderItem={this._renderItem}
                    sliderWidth={Dimensions.get('window').width}
                    itemWidth={300}
                    windowSize={1}
                    />

                <View style={styles.mask_bottom}>
                    <View style={styles.mask_bottom_item}>
                        <TouchableOpacity style={styles.activity} onPress={()=>this.toggleModal(1)}>  
                                <Icon name="male" size={40} color={this.state.distanceCount? '#4bb0bc': '#ccc'} />
                                <View>
                                    <Text>Distance</Text>
                                    <Text style={styles.countStyle}>{this.state.distanceCount}KM</Text>
                                </View>

                        </TouchableOpacity>
                        <TouchableOpacity style={styles.distance} onPress={()=>this.toggleModal(2)}>  
                            <Icon name="male" size={40} color={this.state.stepCount? '#4bb0bc': '#ccc'} />
                            <View>
                                <Text>Step</Text>
                                <Text style={styles.countStyle}>{this.state.stepCount}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.mask_bottom_item}>
                        <TouchableOpacity style={styles.time} onPress={()=>this.toggleModal(3)}>  
                            <Icon name="male" size={40} color={this.state.timeCount? '#4bb0bc': '#ccc'} />
                            <View>
                                <Text>Time</Text>
                                <Text style={styles.countStyle}>{this.state.timeCount}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.calories} onPress={()=>this.toggleModal(4)}>  
                            <Icon name="male" size={40} color={this.state.caloriesCount? '#4bb0bc': '#ccc'} />
                            <View>
                                <Text>Calories</Text>
                                <Text style={styles.countStyle}>{this.state.caloriesCount}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.renderInputModule()}
            
               
                </View>   
            )
        }else if(this.state.status == 0){
            return(
             <View></View>
            )
        }else if(this.state.status == 3){
            return(
                <View style={{flex:1,position:'absolute',width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0.6)',zIndex:100,paddingTop:isIphoneX()?40:0,alignItems:'center',paddingHorizontal:20}}>

                    
                </View>
            )
        }
    }


  render() {

    
    return (
        <Provider >
            {this.renderMask()}
     
            <View style={styles.container}>
                <MapView
                    style={{flex:1,position:'absolute',width:'100%',height:'100%'}}
                    initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                    }}
                />

                <View style={{padding:20}}>
                    <View style={styles.card}>
                        {/* <View style={styles.card_title}>
                            <Text>離每日目標還差1,609步</Text>
                            <Text style={styles.sp_word} onPress={() => this.setModalVisible()}>Start Now</Text>
                        </View> */}
                        <View style={{width:'100%',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
                        <View style={{width:200}}>
                            <Slider value={0.1} disabled minimumTrackTintColor={'#4baec1'} thumbStyle={{height:0}}/>
                        </View>
                        </View>
                        <View style={{flexDirection:'row',alignItems:'baseline',marginBottom:15}}>
                            <Text style={{fontSize:25,fontWeight:"bold",marginRight:5}}>6,391</Text>
                            <Text>step</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%'}}>
                        <View style={styles.data_item}>
                            <Text style={styles.data_item_t}>1.2</Text>
                            <Text style={styles.data_item_b}>distance</Text>
                            <Text style={styles.data_item_b}>(km)</Text>
                        </View>
                        <View style={styles.data_item}>
                            <Text style={styles.data_item_t}>43</Text>
                            <Text style={styles.data_item_b}>Calories</Text>
                            <Text style={styles.data_item_b}>(cal)</Text>
                        </View>
                        <View style={styles.data_item}>
                            <Text style={styles.data_item_t}>37</Text>
                            <Text style={styles.data_item_b}>Time</Text>
                            <Text style={styles.data_item_b}>(min)</Text>
                        </View>
                        </View>
                    </View>
                </View>
                <View style={{position:'absolute',bottom:30,left:40}}>
                    <Button
                    containerStyle={{width:100,backgroundColor:'#2c4b50'}}
                    titleStyle={{color:'#fff'}}
                    title="STOP"
                    onPress={()=>this.stop()}
                    type="outline"
                    />
                </View>
                <View style={{position:'absolute',bottom:30,right:40}}>
                    <Button
                    containerStyle={{width:100,backgroundColor:'#efb166'}}
                    titleStyle={{color:'#fff'}}
                    title="PAUSE"
                    type="outline"
                    />
                </View>
            </View>
        </Provider>
        
    );
  }
}

const styles =  StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    card:{
        width:'100%',
        backgroundColor:'#fff',
        paddingHorizontal:30,
        paddingVertical:20,
        alignItems:'center',
        borderRadius:10,
        // marginBottom:30,
    },
    card_title:{
        flexDirection:'row',
        marginBottom:20
    },
    sp_word:{
      marginLeft:5,
      width:80,
      height:20,
      borderRadius:10,
      textAlign:'center',
      color:'#4bb0bc',
      borderColor:'#4bb0bc',
      borderWidth:1
    },
    close:{
        fontSize:80,
        fontWeight:'bold',
        color:'#fff'
        // position:'absolute',
        // top:'50%'
    },
    mask_bottom:{
        position:'absolute',
        bottom:40,
        backgroundColor:'#fff',
        height:200,
        width:'100%',
        borderRadius:10,
    },
    mask_bottom_item:{
        flexDirection:'row'
    },
    activity:{
        justifyContent:'space-around',
        flexDirection:'row',
        alignItems:'center',
        height:100,
        padding:20,
        width:'50%',
        borderBottomWidth:1,
        borderRightWidth:1,
        borderColor:'#4bb0bc'
    },
    time:{
        justifyContent:'space-around',
        flexDirection:'row',
        alignItems:'center',
        height:100,
        padding:20,
        width:'50%',
        borderRightWidth:1,
        borderColor:'#4bb0bc'
    },
    distance:{
        justifyContent:'space-around',
        flexDirection:'row',
        alignItems:'center',
        height:100,
        padding:20,
        width:'50%',
        borderBottomWidth:1,
        borderColor:'#4bb0bc'
    },
    calories:{
        justifyContent:'space-around',
        flexDirection:'row',
        alignItems:'center',
        height:100,
        padding:20,
        width:'50%',
    },
    modal_content:{
        width:300,
        height:200,
        borderRadius:10,
        backgroundColor:"#fff",
        position:'absolute',
        top:'20%',
        padding:30,
        alignItems:'center'
    },
    btn:{
        marginTop:40,
        height:50,
        fontSize:20,
        backgroundColor:'#4bb0bc',
        color:'#fff'
    },
    countStyle:{
        color:'#4bb0bc',
        fontSize:30,
        fontWeight:'bold',
    },
    carousel:{
        position:'absolute',
        bottom:260
    },
    carouselStyle:{
        width:300,
        height:130,
        paddingTop:5,
        backgroundColor:'rgba(0,0,0,0.6)',
        borderRadius:20,
        alignItems:'center'
    },
    carouseTitle:{
        color:'#fff',
        fontSize:20
    },
    carouseImage:{
        width:300,
        height:100,
        position:'absolute',
        bottom:0,
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20
    },
    now_start:{
        width:100,
        height:100,
        borderRadius:50,
        backgroundColor:'#4bb0bc',
        alignItems:'center',
        justifyContent:'center'
    }
})
