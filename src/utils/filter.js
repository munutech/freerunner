import _MOMENT from 'moment'
import _tz from 'moment-timezone'

// Date
export function formatLongDate(timeDate){
    return _MOMENT(new Date(timeDate)).tz('Asia/Taipei').format("MM/DD/YYYY HH:mm:ss")
  }
  export function formatMDYDate(timeDate){
    if(timeDate){
    return _MOMENT(new Date(timeDate)).utc().format("MM/DD/YYYY")
    }
  }
  export function formatDate(timeDate){
    if(timeDate){
    return _MOMENT(new Date(timeDate)).utc().format("YYYY-MM-DD")
    }
  }
  export function formatMouth(timeDate){
    if(timeDate){
    return _MOMENT(new Date(timeDate)).utc().format("YYYY-MM")
    }
  }