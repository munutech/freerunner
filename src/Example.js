import React, { Component, Fragment } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native'

import moment from 'moment'

const weekTitle = ['日', '一', '二', '三', '四', '五', '六']

export default class Example extends Component{

    constructor(props) {
        super(props)
        this.state = {
            date: new Date()
        }
    }

    _onPress(date) {
        this.setState({
            date
        })
    }

    lastWeek = () => {
        this.setState({
            date: new Date(this.state.date).getTime() - 7 * 24 * 60 * 60 * 1000
        })
    }

    nextWeek = () => {
        this.setState({
            date: new Date(this.state.date).getTime() + 7 * 24 * 60 * 60 * 1000
        })
    }

    render() {
        let days = [] // 存放节点
        let date = new Date(this.state.date)
        let num = 7 - date.getDay() // 今天 分割点
        for (let i = 7 - num; i > 0; i--) { // 今天以前的日期
            let nowDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - i)
            let fmtNow = moment(nowDate).format('YYYY-MM-DD');
            let node = <TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st]}>{nowDate.getDate()}</Text></TouchableOpacity>
            if (fmtNow === moment(date).format('YYYY-MM-DD')) {
                node = <View style={styles.curr}><TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st]}>{nowDate.getDate()}</Text></TouchableOpacity></View>
            }
            days.push(node)
        }
        for (let i = 0; i < num; i++) { // 今天以后的日期 含今天
            let nowDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + i)
            let fmtNow = moment(nowDate).format('YYYY-MM-DD');
            let node = <TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st]}>{nowDate.getDate()}</Text></TouchableOpacity>
            if (fmtNow === moment(date).format('YYYY-MM-DD')) {
                node = <View style={styles.curr}><TouchableOpacity onPress={this._onPress.bind(this, fmtNow)}><Text style={[styles.fs, styles.st, { color: '#FFFFFF' }]}>{nowDate.getDate()}</Text></TouchableOpacity></View>
            }
            days.push(node)
        }

        return (
            <Fragment>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ width: Dimensions.get('window').width / 2, textAlign: 'center', color: '#316DE6', fontSize: 15 }} onPress={this.lastWeek}>上一周</Text>
                    <Text style={{ width: Dimensions.get('window').width / 2, textAlign: 'center', color: '#316DE6', fontSize: 15 }} onPress={this.nextWeek}>下一周</Text>
                </View>
                <View style={[styles.container01, styles.space]}>
                    <View style={styles.weekTitleContainer}>
                        {
                            weekTitle.map((item) => (
                                <View>
                                    <Text style={[styles.fs, styles.st]}>{item}</Text>
                                </View>
                            ))
                        }
                    </View>
                    <View style={[styles.weekTitleContainer, styles.fs]}>
                        {days}
                    </View>
                </View>
            </Fragment>
        )
    }

}
const styles = StyleSheet.create({
    weekTitleContainer: {
        height: 27,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    fs: {
        fontSize: 13,
    },
    curr: {
        borderRadius: 24,
        backgroundColor: '#316DE6',
        width: 24,
        height: 24,
        textAlign: 'center',
        lineHeight: 24,
    },
    st: {
        width: 24,
        height: 24,
        textAlign: 'center',
        lineHeight: 24,
    },
    space: {
        flex: 1,
        paddingTop: 10,
        paddingHorizontal: 30,
        paddingBottom: 10
    }
})


