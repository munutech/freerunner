import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Toast,
  Alert
} from 'react-native';
import {isIphoneX, ifIphoneX} from './utils/isIphonex'
import { Slider, Button, ButtonGroup } from 'react-native-elements';
import MapView,{Polyline} from 'react-native-maps';
import { Actions } from 'react-native-router-flux';
import { ShareDialog,ShareApi } from 'react-native-fbsdk';
import { captureScreen, captureRef } from 'react-native-view-shot';
export default class Example extends Component {
	constructor(props){
		super(props)
		this.state={
			shareLinkContent:{
				contentType: 'link',
				contentUrl: "https://facebook.com",
				contentDescription: 'Wow, check out this great site!',
            },
            makingImage:false,
            imageUrl:''
        }
        this.mainViewRef = React.createRef();
    }

    componentDidMount(){
        this.shareScreenShot()

    }

   
    async shareScreenShot() {
        console.log(11)
        const { makingImage } = this.state;
        if (makingImage) return;
    
        this.setState({ makingImage: true }, async () => {
          try {
            const captureConfig = {
              format: 'png',
              quality: 0.7,
              // result: Platform.OS==='ios'? 'data-uri':'base64',
              // result: 'tmpfile',
              result: 'base64',
              width: 750,
            };
            let imgBase64 = '';
            try {
              imgBase64 = await captureScreen(captureConfig);
            } catch (e) {
              try {
                imgBase64 = await captureRef(this.mainViewRef.current, captureConfig);
              } catch (ex) {
                throw ex;
              }
            }
            this.imgBase64 = imgBase64;
            this.setState({ showTitle: true });
            const screenShotShowImg = `data:image/png;base64,${this.imgBase64}`;
            this.setState({
                imageUrl:screenShotShowImg
            })
            console.log(screenShotShowImg)
            //FIXME screenShotShowImg可直接在Image中展示
            // console.log('this.screenShotShowImg====', this.screenShotShowImg);
            // this.saveImage(screenShotShowImg);   
          } catch (e) {
            Alert.alert(`截图失败，请稍后再试${e.toString()}`);
            console.log(e);
          } finally {
            this.setState({ makingImage: false });
          }
        });
      }
    //   const sharePhotoContent = {
    //     contentType = 'photo',
    //     photos: [{ imageUrl: photoUri }],
    //   }
      
      
  // Share the link using the share dialog.
	shareLinkWithShareDialog = () =>{
        
        var tmp = this;
        const sharePhotoContent = {
            contentType: 'photo',
            photos: [{ imageUrl: this.state.imageUrl }],
        }
		ShareDialog.canShow(sharePhotoContent).then(
			function(canShow) {
				if (canShow) {
					return ShareDialog.show(sharePhotoContent);
				}
			}
		).then(
			function(result) {
				if (result.isCancelled) {
					console.log('Share cancelled');
				} else {
					console.log('Share success with postId: '
						+ result.postId);
				}
			},
			function(error) {
				console.log('Share fail with error: ' + error);
			}
		);
	}
  render() {

    return (
        <View style={styles.container} ref={this.mainViewRef}>
            <View style={{padding:20}}>
                <Text style={{color:'#c8c8c8',fontSize:20}}>Today 21:00</Text>
                <Text style={{fontSize:30,color:'#fff'}}>夜間慢跑</Text>
            </View>
            {/* <MapView
                    style={{width:'100%',height:400}}
                    initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                    }}
                /> */}
            <MapView 
                style={{width:'100%',height:'50%'}} 
                initialRegion={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
                }}
            >
            <Polyline
                coordinates={[
                    { latitude: 37.8025259, longitude: -122.4351431 },
                    { latitude: 37.7896386, longitude: -122.421646 },
                    { latitude: 37.7665248, longitude: -122.4161628 },
                    { latitude: 37.7734153, longitude: -122.4577787 },
                    { latitude: 37.7948605, longitude: -122.4596065 },
                    { latitude: 37.8025259, longitude: -122.4351431 },
                    { latitude: 37.8025254, longitude: -122.4351433 },
                    { latitude: 37.8045955, longitude: -122.4351432 },
                    { latitude: 37.8025256, longitude: -122.4351436 },
                ]}
                strokeColor="red" // fallback for when `strokeColors` is not supported by the map-provider
                // strokeColors={[
                // 	'#7F0000',
                // 	'#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                // 	'#B24112',
                // 	'#E5845C',
                // 	'#238C23',
                // 	'#7F0000'
                // ]}
                strokeWidth={5}
            />
        </MapView>   
            <View style={styles.detail}>
                <View style={styles.item}>
                    <Text style={styles.title}>距离</Text>
                    <View style={styles.data}>
                        <Text style={styles.num}>1.13</Text>
                        {/* <Text style={styles.title}>(km)</Text> */}
                    </View>
                </View>
                <View style={styles.item}>
                    <Text style={styles.title}>卡洛里</Text>
                    <View style={styles.data}>
                        <Text style={styles.num}>151</Text>
                        {/* <Text style={styles.title}>(cal)</Text> */}
                    </View>
                </View>
                <View style={styles.item}>
                    <Text style={styles.title}>時間</Text>
                    <View style={styles.data}>
                        <Text style={styles.num}>412</Text>
                        {/* <Text style={styles.title}>(min)</Text> */}
                    </View>
                </View>
                <View style={styles.item}>
                    <Text style={styles.title}>步数</Text>
                    <View style={styles.data}>
                        <Text style={styles.num}>412</Text>
                        {/* <Text style={styles.title}>(min)</Text> */}
                    </View>
                </View>
            </View>
            <View style={{position:'absolute',bottom:30,left:40}}>
                <Button
                containerStyle={{width:100,backgroundColor:'#49aec0'}}
                titleStyle={{color:'#fff'}}
                title="分享"
                onPress={()=>this.shareLinkWithShareDialog()}
                type="outline"
                />
            </View>
            <View style={{position:'absolute',bottom:30,right:40}}>
                <Button
                onPress={()=>Actions.home()}
                containerStyle={{width:100,backgroundColor:'#efb166'}}
                titleStyle={{color:'#fff'}}
                title="完成"
                type="outline"
                />
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    //   paddingTop:isIphoneX()?40:0,
      backgroundColor: '#232323',
    },
    detail:{
        flexDirection:'row',
        justifyContent:'space-around',
        marginTop:20
    },
    item:{
        alignItems:'center'
    },
    data:{
        flexDirection:'row',
        alignItems:'baseline'
    },
    title:{
        color:'#fff'
    },
    num:{
        fontSize:40,
        color:'#fff',
        fontWeight:'bold'
    }
})