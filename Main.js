
import React,{Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  DeviceEventEmitter,
} from 'react-native';
import {Scene,ActionConst, Router, Actions,Stack} from 'react-native-router-flux'
import App from './App'
import Home from './src/Home'
import Login from './src/Login'
import Mine from './src/Mine'
import History from './src/History'
import Register from './src/Register'
import Addstepone from './src/Addstepone'
import Addstepthree from './src/Addstepthree'
import Addsteptwo from './src/Addsteptwo'
import CameraQr from './src/Camera'
import Start from './src/Start'
import Result from './src/Result'
import Icon from 'react-native-vector-icons/FontAwesome';
import List from './src/List'
import Exercising from './src/Exercising'
import Example from './src/Example'


export default class Main extends Component{
  constructor(props){
    super(props)
    this.state={

    }
  }
  componentDidMount(){  
   
  }
 
  componentWillUnmount(){

  }

  UNSAFE_componentWillMount(){

  }

  editstepone = () => {
    DeviceEventEmitter.emit('isEditNotice',1)
  }

  editstepthree = () => {

    console.log(this.state.objectData)
    // DeviceEventEmitter.emit('getValue',this.state)

  }




  render() {
        return (  
            <Router>
                <Scene key="root">
                  <Scene key="login" component={Login} title="登陸" />
                  <Scene key="register" component={Register} title="登陸" />
                  <Scene key="home" component={Home} title={'首頁'} 
                      renderLeftButton={
                       <View style={styles.leftButtonStyle}>
                         <Text onPress={()=>this.editstepone()}>
                            <Icon name='plus' size={25} color='#000' onPress={()=>Actions.addstepone()}/>
                         </Text>
                       </View>}
                  />
                  <Scene key="app" component={App} title={'首頁'} hideNavBar={true}/>
                  <Scene key="mine" component={Mine} title={'我的'} />
                  <Scene key="addstepone" component={Addstepone} hideNavBar={false} title="新增追蹤物件" 
                      type={ActionConst.RESET}
                      renderLeftButton={
                        <View style={styles.leftButtonStyle}>
                          <Icon name='close' size={25} color='#000' onPress={()=>Actions.home()}/>
                        </View>
                      }
                       renderRightButton={
                        <View style={styles.leftButtonStyle}>
                          <Text onPress={()=>this.editstepone()}>
                            下一步
                          </Text>
                        </View>
                      }                    
                    />
                    <Scene key="addsteptwo" component={Addsteptwo} title="新增追蹤物件" 
                      type={ActionConst.RESET}
                      renderLeftButton={
                        <View style={styles.leftButtonStyle}>
                          <Icon name='close' size={25} color='#000' onPress={()=>Actions.home()}/>
                        </View>
                      }
                    />
                    <Scene key="addstepthree" component={Addstepthree} title="新增追蹤物件" 
                      type={ActionConst.RESET}
                      renderLeftButton={
                        <View style={styles.leftButtonStyle}>
                          <Icon name='close' size={25} color='#000' onPress={()=>Actions.home()}/>
                        </View>
                      }
                      renderRightButton={
                        <View style={styles.leftButtonStyle}>
                          <Text onPress={()=>this.editstepthree()}>
                            確認
                          </Text>
                        </View>
                      }
                    />
                    <Scene key="scan" component={CameraQr} title="扫一扫" />
                    <Scene key="start" component={Start} title="開始" />
                    <Scene key="result" component={Result} title="Result" hideNavBar={true}/>
                    <Scene key="exercising" component={Exercising} title="Exercising" hideNavBar={true}/>
                    <Scene key="list" component={List} title="List" />
                    <Scene key="example" component={Example} title="Example" />
                    <Scene key="history" component={History} title="History" />

                </Scene>
            </Router>

          );
  }
}
  

  const styles = StyleSheet.create({
    leftButtonStyle:{
      paddingHorizontal:10
    }

  })